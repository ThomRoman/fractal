#include <math.h>                            //para usar la funcion seno y coseno
#define PI 3.1415926535897932384626433832795 //definimos PI
#include <GL/glut.h>                         //usamos la libreria Glut

//variables de apoyo
int n = 6;                   //numero de iteraciones
double w;                    //angulo
float division_radio = 2.35; //el porcentaje de reduccion de cada circulo en cada iteracion

//FUNCION CIRCULO
//aqui se dibuja el circulo
void circulo(float radio)
{
    glColor3f(1, 0.3882, 0.2784); //color de la circunferencia
    glBegin(GL_POLYGON);          //funcion para dibujar poligonos
    //utilizamos un for para hallar los puntos del contorno del circulo
    for (w = 0; w < 2 * PI; w += PI / 10)
        glVertex3f(cos(w) * radio, sin(w) * radio, 0.0); //graficamos las coordenadas del circulo
    glEnd();
}

//FUNCION DIVIDIR CIRCULO
//aqui se realiza los dibujos del circulo y sus otros circulos a su alrededor

void dividir_circulo(float radio, float translacionX, float translacionY, int m)
{
    //float radio: el valor del radio que tendra la circunferencia
    //float translacionX:  el valor de la translacion que tendra el circulo el eje X
    //float translacionY:  el valor de la translacion que tendra el circulo el eje Y
    //int m: el valor del numero de iteraciones

    //mientras la iteracion no sea cero, el algoritmo sigue dibujando
    if (m > 0)
    {

        //MATRIZ PRINCIPAL
        //matriz donde se dibujan los cinco circulos (EL CIRCULO PRINCIPAL Y LOS CUATRO CIRCULOS SECUNDARIOS
        glPushMatrix();
        glTranslatef(translacionX, translacionY, 0); //aqui se hace el translado, que afecta a los cincos circulos

        //MATRIZ CIRCULO CENTRO
        //matriz donde se dibuja el circulo del centro
        glPushMatrix();
        //utilizamos la funcion circulo para hacer el dibujo
        circulo(radio);
        glPopMatrix();

        //MATRIZ CIRCULO DERECHO
        //matriz donde se dibuja el circulo de la derecha
        glPushMatrix();
        //hacemos un traslado para que el circulo se posicione a la derecha del circulo principal
        glTranslatef(radio + (radio / division_radio), 0, 0);
        //utilizamos la funcion circulo para hacer el dibujo
        //pero esta vez con un radio menor
        circulo(radio / division_radio);
        glPopMatrix();

        //MATRIZ CIRCULO ARRIBA
        //matriz donde se dibuja el circulo de arriba
        glPushMatrix();
        //hacemos un traslado para que el circulo se posicione arriba del circulo principal
        glTranslatef(0, radio + (radio / division_radio), 0);
        //utilizamos la funcion circulo para hacer el dibujo
        //pero esta vez con un radio menor
        circulo(radio / division_radio);
        glPopMatrix();

        //MATRIZ CIRCULO IZQUIERDO
        //matriz donde se dibuja el circulo de la izquierda
        glPushMatrix();
        //hacemos un traslado para que el circulo se posicione a la izquierda del circulo principal
        glTranslatef(-(radio + (radio / division_radio)), 0, 0);
        //utilizamos la funcion circulo para hacer el dibujo
        //pero esta vez con un radio menor
        circulo(radio / division_radio);
        glPopMatrix();

        //MATRIZ CIRCULO ABAJO
        //matriz donde se dibuja el circulo de abajo
        glPushMatrix();
        //hacemos un traslado para que el circulo se posicione abajo del circulo principal
        glTranslatef(0, -(radio + (radio / division_radio)), 0);
        //utilizamos la funcion circulo para hacer el dibujo
        //pero esta vez con un radio menor
        circulo(radio / division_radio);
        glPopMatrix();
        glPopMatrix();

        //RECURSIVIDAD
        //utilizamos la misma funcion para los cuatro circulos secundarios:

        //RECURSIVIDAD PARA EL CIRCULO DERECHO, siendo esta vez, el circulo principal
        dividir_circulo(radio / division_radio, translacionX + radio + (radio / division_radio), translacionY + 0, m - 1); //la iteracion se reduce en 1

        //RECURSIVIDAD PARA EL CIRCULO DE ARRIBA, siendo esta vez, el circulo principal
        dividir_circulo(radio / division_radio, translacionX + 0, translacionY + radio + (radio / division_radio), m - 1);

        //RECURSIVIDAD PARA EL CIRCULO IZQUIERDO, siendo esta vez, el circulo principal
        dividir_circulo(radio / division_radio, translacionX + -(radio + (radio / division_radio)), translacionY + 0, m - 1);

        //RECURSIVIDAD PARA EL CIRCULO DE ABAJO, siendo esta vez, el circulo principal
        dividir_circulo(radio / division_radio, translacionX + 0, translacionY + -(radio + (radio / division_radio)), m - 1);
    }
}

//FUNCION MOSTRA
//esta funcion llama a la funcion anterior para que muestre el algoritmo en pantalla
void display()
{
    glClear(GL_COLOR_BUFFER_BIT);
    dividir_circulo(100, 0, 0, n);
    //en este caso el radio de la circunferencial inicila es 100
    //el translado en el eje X es 0
    //el translado en el eje Y es 0
    //y se haran n iteraciones
    glFlush();
}

//FUNCION PANTALLA
//en esta funcion definimos el color de fondo y la lejania de la camara
void myinit()
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(-300.0, 300.0, -300.0, 300.0);
    glMatrixMode(GL_MODELVIEW);
    glClearColor(1.0, 1.0, 1.0, 1.0);
    glColor3f(0.0, 0.0, 0.0);
}

//FUNCION PRINCIPAL
int main(int argc, char **argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(500, 500);
    glutCreateWindow("Fragmento Circular");
    glutDisplayFunc(display);
    myinit();
    glutMainLoop();
}
